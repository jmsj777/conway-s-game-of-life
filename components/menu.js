import Configs from "./configs.js"

const template = document.getElementById("t-menu")

class Menu extends HTMLElement {
  constructor() {
    super()
    this.appendChild(template.content.cloneNode(true))
    this.rootStyle = document.documentElement.style

    this.header = this.querySelector("#header")
    this.toggleSettings = this.querySelector("#toggleSettings")
    this.pos1 = 0
    this.pos2 = 0
    this.pos3 = 0
    this.pos4 = 0
    this.playButton = this.querySelector("play-button-component")
  }

  connectedCallback() {
    this.header.onmousedown = this.dragMouseDown
    this.toggleSettings.addEventListener("click", this.openModal.bind(this))
  }

  dragMouseDown(e) {
    e = e || window.event
    e.preventDefault()
    // get the mouse cursor position at startup:
    this.parentElement.pos3 = e.clientX
    this.parentElement.pos4 = e.clientY

    document.onmouseup = closeDragElement
    // call a function whenever the cursor moves:
    document.onmousemove = elementDrag
  }
  openModal() {
    let modal = document.querySelector("modal-component")
    if (modal) {
      modal.visible ? modal.hide() : modal.show()
    } else document.body.appendChild(new Configs())

    if (this.playButton.active) {
      this.playButton.click()
    }
  }
}

function elementDrag(e) {
  e = e || window.event
  e.preventDefault()
  let menu = e.target.parentElement
  // calculate the new cursor this.position:
  menu.pos1 = menu.pos3 - e.clientX
  menu.pos2 = menu.pos4 - e.clientY
  menu.pos3 = e.clientX
  menu.pos4 = e.clientY
  // set the element's new position:
  menu.style.top = `${menu.offsetTop - menu.pos2}px`
  menu.style.left = `${menu.offsetLeft - menu.pos1}px`
}

function closeDragElement(e) {
  // stop moving when mouse button is released:
  document.onmouseup = null
  document.onmousemove = null
}

customElements.define("menu-component", Menu)
export default Menu
