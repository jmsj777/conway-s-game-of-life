import Block from "./block.js"
import RootHandler from "./utils/rootHandler.js"

const template = document.getElementById("t-grid")

class Grid extends HTMLElement {
  constructor() {
    super()
    this.appendChild(template.content.cloneNode(true))

    this.blHeight = this.getAttribute("blHeight") || 15
    this.lines = parseInt(this.getAttribute("proportion"))
    if (this.lines) {
      this.columns = this.lines
    } else {
      ;[this.lines, this.columns] = this.identifyProportion()
    }
    this.avgExTime = {
      value: 0,
      count: 0,
    }

    this.threshold = 3
    this.frequencyMs = 200
  }
  connectedCallback() {
    for (let i = 0; i < this.lines; i++) {
      for (let j = 0; j < this.columns; j++) {
        this.appendChild(new Block())
      }
    }

    const rh = new RootHandler()
    rh.setLines(this.lines)
    rh.setColumns(this.columns)
    rh.setBlHeight(this.blHeight)
  }

  identifyProportion() {
    return [
      Math.floor(window.innerHeight / this.blHeight),
      Math.floor(window.innerWidth / this.blHeight),
    ]
  }

  simulate() {
    this.intervalId = setInterval(this.epoch.bind(this), this.frequencyMs)
  }
  epoch() {
    let now = Date.now()
    let blocks = document.querySelectorAll("block-component")
    let actions = []
    for (let i = 0; i < blocks.length; i++) {
      let nearAlive = this.neigborhood(blocks, i)
      if (blocks[i].active) {
        if (nearAlive < this.threshold - 1 || nearAlive > this.threshold) {
          actions.push({
            block: blocks[i],
            newState: false,
          })
          // blocks[i].blockOff()
        }
      } else {
        if (nearAlive === this.threshold) {
          // blocks[i].blockOn()
          actions.push({
            block: blocks[i],
            newState: true,
          })
        }
      }
    }

    actions.map(({ block, newState }) => {
      block.active = newState ? block.blockOn() : block.blockOff()
    })
    let duration = Date.now() - now
    this.avgExTime.count++
    this.avgExTime.value =
      (this.avgExTime.value * (this.avgExTime.count - 1) + duration) /
      this.avgExTime.count
  }
  pauseSimulation() {
    clearInterval(this.intervalId)
  }

  neigborhood(blocks, i) {
    let neighbours = [
      blocks[i - this.columns - 1],
      blocks[i - this.columns],
      blocks[i - this.columns + 1],
      blocks[i - 1],
      blocks[i + 1],
      blocks[i + this.columns - 1],
      blocks[i + this.columns],
      blocks[i + this.columns + 1],
    ]
    return neighbours.reduce((acc, i) => {
      return typeof i !== "undefined" && i.active ? acc + 1 : acc
    }, 0)
  }
}

customElements.define("grid-component", Grid)
export default Grid
