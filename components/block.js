const template = document.getElementById("t-block")

class Block extends HTMLElement {
  constructor() {
    super()
    this.appendChild(template.content.cloneNode(true))
    // this.innerHTML = "&nbsp;"
    this.active = false
    // this.innerHTML = "x"
  }

  connectedCallback() {
    this.addEventListener("click", this.onClick)
  }

  onClick(e) {
    this.active = this.active ? this.blockOff() : this.blockOn()
  }

  blockOn() {
    this.classList.add("active")
    return true
  }
  blockOff() {
    this.classList.remove("active")
    return false
  }
}

customElements.define("block-component", Block)
export default Block
