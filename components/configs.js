const template = document.getElementById("t-configs")

class Configs extends HTMLElement {
  constructor() {
    super()
    this.appendChild(template.content.cloneNode(true))

    this.visible = true
    this.blHeight = this.querySelector("#blHeight")
    this.lines = this.querySelector("#lines")
    this.columns = this.querySelector("#columns")
    this.frequencyHz = this.querySelector("#frequencyHz")
    this.btSubmit = this.querySelector("#btSubmit")
    this.btClose = this.querySelector("#btClose")
    this.btFitToWindow = this.querySelector("#btFitToWindow")
    this.btDarkTheme = this.querySelector("#btDarkTheme")
  }

  connectedCallback() {
    const grid = document.querySelector("grid-component")
    this.blHeight.value = grid.blHeight
    this.lines.value = grid.lines
    this.columns.value = grid.columns
    this.frequencyHz.value = 1000 / grid.frequencyMs

    this.btSubmit.addEventListener("click", this.submit.bind(this))
    this.btClose.addEventListener("click", this.hide.bind(this))
    this.btFitToWindow.addEventListener(
      "click",
      this.toggleFitToWindow.bind(this)
    )
    this.btDarkTheme.addEventListener("click", this.toggleDarkTheme.bind(this))
  }

  show() {
    this.classList.remove("hidden")
    this.visible = true
  }
  hide() {
    this.classList.add("hidden")
    this.visible = false
  }

  submit() {
    const grid = document.querySelector("grid-component")
    grid.frequencyMs = 1000 / this.frequencyHz.value

    grid.blHeight = parseInt(this.blHeight.value)

    if (this.btFitToWindow.checked) {
      ;[this.lines.value, this.columns.value] = grid.identifyProportion()
    }
    if (
      grid.lines != this.lines.value ||
      grid.columns != this.columns.value ||
      grid.blHeight != this.blHeight.value
    ) {
      grid.lines = parseInt(this.lines.value)
      grid.columns = parseInt(this.columns.value)
      grid.innerHTML = ""
      grid.connectedCallback()
    }
    this.hide()
  }

  toggleFitToWindow() {
    const grid = document.querySelector("grid-component")
    if (this.btFitToWindow.checked) {
      this.lines.disabled = true
      this.lines.value = ""
      this.columns.disabled = true
      this.columns.value = ""
      grid.blHeight = parseInt(this.blHeight.value)
      ;[this.lines.value, this.columns.value] = grid.identifyProportion()
    } else {
      this.lines.disabled = false
      this.lines.value = grid.lines
      this.columns.disabled = false
      this.columns.value = grid.columns
    }
  }

  toggleDarkTheme(e) {
    this.btDarkTheme.checked
      ? document.documentElement.classList.add("dark")
      : document.documentElement.classList.remove("dark")
  }
}

customElements.define("configs-component", Configs)
export default Configs
